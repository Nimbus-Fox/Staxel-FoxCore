﻿function addJs(script: string) {
    $("head").append($("<script>").append(script));
}

function addCss(css: string) {
    $("head").append($("<stylesheet>").append(css));
}

function addHtml(html: string) {
    $("body").append(html);
}

$(document).keyup((event) => {
    if (event.keyCode === 27) {
        (window as any).hide();
    }
});
console.log("Checking jquery");